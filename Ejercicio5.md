# Cancelando una tarea

Cuando se ejecuta una tarea [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html) en un **marco Fork/Join**, se puede cancelar la misma antes de que empiece su ejecución. La clase `ForkJoinTask` proporciona el método `cancel(.)` para este propósito. Hay ciertos elementos que hay que tener presente cuando queremos cancelar una tarea, que son las siguientes:

- La clase [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html) no proporciona ningún método para cancelar todas las tareas que se están ejecutando o esperando en el marco.

- Cuando se cancela una tarea, no se pueden cancelar las tareas que esta tarea ha ejecutado.

En este ejemplo, se implementará un método para cancelar la ejecución de un objeto `ForkJoinTask`. El ejemplo consiste en  buscar la posición de un elemento, un número en nuestro caso, en un array. La primera tarea que encuentra el elemento cancelará el resto de las tareas. Como esta funcionalidad no está implementada en el **marco Fork/Join**, se implementará una clase auxiliar para hacer esta cancelación.

1. Definimos una clase llamada `ArrayGenerator`. Esta clase genera un array que contiene enteros que se han generado aleatoriamente. El tamaño del array se deberá pasar al método `generateArray(.)`. Revisar el código de ejemplo del guión.

2. Definimos una clase llamada `TaskManager`. Usamos esta clase para almacenar todas las tareas que se ejecutan en el **marco Fork/Join** usados en el ejemplo. Por las limitaciones de las clases `ForkJoinPool` y `ForkJoinTask`, utilizaremos esta clase para cancelar las tareas del **marco Fork/Join**.

3. Tenemos una variable de instancia que almacenará las tareas que se ejecutan. En el constructor se creará el array. Además necesitaremos un método llamado `addTask(.)` que añadirá las tareas a la variable de instancia.

4. Implementamos un método `cancelTasks(.)`. Se cancelarán todas las tareas que se encuentran almacenadas en la variable de instancia `task`. Se pasará como parámetro la tarea que ha solicitado la cancelación para que se solicite la cancelación del resto que se encuentran almacenadas.

```java
...
/**
 * Method that cancel all the tasks in the list
 * @param cancelTask 
 */
public void cancelTasks(ForkJoinTask<Integer> cancelTask){
    for (ForkJoinTask<Integer> task  :tasks) {
        if (task!=cancelTask) {
            task.cancel(true);
            ((SearchNumberTask)task).writeCancelMessage();
        }
    }
}
...
```

5. Para implementar la tarea de búsqueda utilizamos la clase llamada `SearchNumberTask` que hereda de la clase `RecursiveTask` y está *parametrizada* por la clase `Integer` que es el valor del resultado devuelto por la tarea.

6. Como variables de instancia necesitamos:
	- El array donde se buscará el número.
	- Dos enteros para establecer los índices entre los que se buscará.
	- Un entero para almacenar el número que se quiere encontrar.
	- Un objeto de la clase `TaskManager` que permitirá cancelar la ejecución de las tareas.
    
7. El constructor de la clase establecerá el valor de las variables de instancia para la realización de la tarea.

8. Implementamos el método `compute()` para realizar las operaciones propias de la tarea. El umbral del tamaño de la tarea será el de `10` números. Utilizamos los métodos `launchTasks()` para añadir nuevas tareas al marco y el método `lookForNumber()` para encontrar el número.

```java
...
/**
 * If the block of number this task has to process has more than
 * ten elements, divide that block in two parts and create two
 * new Tasks using the launchTasks() method.
 * Else, looks for the number in the block assigned to it using
 * the lookForNumber() method
 */
@Override
protected Integer compute() {
    System.out.println("Task: "+start+":"+end);
    int ret;
    if (end-start>10) {
        ret=launchTasks();
    } else {
        ret=lookForNumber();
    }
    return new Integer(ret);
}
...
```

9. Implementamos el método `lookForNumber()`. Se comprobará si en el bloque del array se encuentra el número que se busca. Si es así, se presenta el mensaje por la salida estándar y se pide la cancelación del resto de las tareas y se devuelve la posición donde se encuentra. Para simular el tiempo de búsqueda, se suspende la tarea por un segundo antes de comprobar otro elemento del array. Si no se encuentra, se devuelve un valor previamente establecido para indicar este hecho.

```java
...
/**
 * Looks for the number in the block of numbers assigned to this task
 * @return The position where it found the number or -1 if it doesn't find it
 */
private int lookForNumber() {
    for (int i=start; i<end; i++){
        if (numbers[i]==number) {
            System.out.printf("Task: Number %d found in position %d\n",number,i);
            manager.cancelTasks(this);
            return i;
        }
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    return NOT_FOUND;
}
...
```

10. Implementamos el método `launchTasks()`. Dividirá es bloque de búsqueda en dos mitades y creará dos tareas para realizar la búsqueda. Las tareas se añadirán las tareas al objeto de la clase `TaskManager` para su cancelación, por si fuera necesario.

11. Añadimos las tareas al **marco Fork/Join** de forma **asíncrona**. Esperamos por la finalización de la primera tarea. Si ha encontrado el número lo devolvemos como resultado de nuestra tarea. Si no, esperamos a la finalización de la segunda y devolvemos su valor como resultado de la tarea.

```java
...
/**
 * Divide the block of numbers assigned to this task in two and 
 * execute to new Task objects to process that blocks 
 * @return The position where the number has been found of -1
 * if the number haven't been found in the subtasks
 */
private int launchTasks() {
    int mid=(start+end)/2;
		
    SearchNumberTask task1=new SearchNumberTask(numbers,start,mid,number,manager);
    SearchNumberTask task2=new SearchNumberTask(numbers,mid,end,number,manager);
		
    manager.addTask(task1);
    manager.addTask(task2);

    task1.fork();
    task2.fork();
    int returnValue;
		
    returnValue=task1.join();
    if (returnValue!=-1) {
        return returnValue;
    }
		
    returnValue=task2.join();
    return returnValue;
}
...
```

12. Implementamos el método `main(.)` de nuestra aplicación para probar el resultado de la tarea de búsqueda que hemos diseñado.

13. Generamos un array de `1000` números, creamos un objeto de la clase `TaskManager`, creamos un objeto de la clase `Task` con el array generado. Por último, creamos el **marco Fork/Join** y añadimos la tarea de búsqueda.

14. Finalizamos la ejecución del **marco Fork/Join** y esperamos a la finalización de la tarea de búsqueda.

15. Por último presentamos por la salida estándar un mensaje indicando que hemos finalizado.

```java
...
public static void main(String[] args) {

    // Generate an array of 1000 integers
    ArrayGenerator generator=new ArrayGenerator();
    int array[]=generator.generateArray(1000);
		
    // Create a TaskManager object
    TaskManager manager=new TaskManager();
		
    // Create a ForkJoinPool with the default constructor
    ForkJoinPool pool=new ForkJoinPool();
		
    // Create a Task to process the array
    SearchNumberTask task=new SearchNumberTask(array,0,1000,5,manager);
		
    // Execute the task
    pool.execute(task);

    // Shutdown the pool
    pool.shutdown();
		
    // Wait for the finalization of the task
    try {
        pool.awaitTermination(1, TimeUnit.DAYS);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
		
    // Write a message to indicate the end of the program
    System.out.printf("Main: The program has finished\n");
}
...
```

 
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwNjk2Mzc4NTMsMTg2ODg3OTYwNV19
-->