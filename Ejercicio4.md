# Lanzando excepción en las tareas

Hay dos tipos de excepciones en Java:

- **Excepciones capturadas**: Estas excepciones deben ser especificadas en la cláusula `throws` o el método debe tratarlas internamente. Por ejemplo, `IOException` o `ClassNotFoundException`.

- **Excepciones no capturadas**: Estas excepciones no pueden ser especificadas o tratadas. Por ejemplo, `NumberFormatException`.
 
No se pueden especificar mediante la cláusula `throws` ninguna **excepción capturada** en el método `compute()` de la clase [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html), porque este método no incluye ninguna declaración mediante la cláusula `throws` en sus implementaciones. Se tiene que incluir el código necesario para tratar las excepciones. Por otro lado, se puede lanzar (o puede ser lanzada por cualquier método o objeto utilizado dentro del método `compute()`) una **excepción no capturada**. El comportamiento de las clases `ForkJoinTask` y [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html) es diferente de lo que se puede esperar. El programa no finaliza su ejecución y no se mostrará ninguna información en la consola sobre la excepción. Es como si la excepción no hubiera sido lanzada. Se puede, sin embargo, utilizar algunos métodos de la clase `ForkJoinTask` para conocer si una tarea lanzó una excepción y el tipo de excepción que fue. En este ejemplo, se aprenderá cómo obtener esta información.

1. Definimos una clase llamada `Task` que hereda de la clase [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) y está *parametrizada* por la clase `Integer` que es el tipo que devuelve la tarea a su finalización.

2. Para la tarea necesitamos tres variables de instancia:
	- Un array de enteros que simulará los datos que la tarea deberá tratar.
	- Dos enteros que indicarán el inicio y el fin de los datos que deberán tratarse.

3. El constructor de la clase deberá establecer los valores apropiados para las variables de instancia. Revisar el código de ejemplo del guión.

4. Implementamos el método `compute()` donde se establecerán los pasos que la tarea debe realizar en su ejecución.

5. Como primer paso se presenta en la salida estándar donde se presenta el espacio que la tarea gestiona.

6. El tamaño umbral para la división de las tareas es de `10` elementos. En caso que se deba resolver la tarea, comprobamos si en el espacio de trabajo se encuentra el índice `4` del array. Si es así, se lanzará una excepción del tipo `RuntimeException`.

```java
...
@Override
protected Integer compute() {
    System.out.printf("Task: Start from %d to %d\n",start,end);
    if (end-start<10) {
        if ((3>start)&&(3<end)){
            throw new RuntimeException("This task throws an Exception: Task from  "+start+" to "+end);
        }
			
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
...
```

7. En caso que el tamaño sea superior al umbral, se dividirá el espacio en dos espacios iguales, creando una tarea para cada uno de ellos, y se añadirán al **marco Fork/Join** de forma sincronizada.

```java
...
    } else {
        int mid=(end+start)/2;
        Task task1=new Task(array,start,mid);
        Task task2=new Task(array,mid,end);
        invokeAll(task1, task2);
        System.out.printf("Task: Result form %d to %d: %d\n",start,mid,task1.join());
        System.out.printf("Task: Result form %d to %d: %d\n",mid,end,task2.join());
    }
    System.out.printf("Task: End form %d to %d\n",start,end);
    return new Integer(0);
}
...
```

8. Se devuelve como resultado de la tarea el resultado `0`.

9. Implementamos el método `main(.)` para realizar una prueba de ejecución de la tarea y comprobar el resultado de lanzar la excepción.
 
10. Creamos un array de enteros de `100` posiciones.

11. Creamos una tarea para tratar el array completo.

12. Creamos el **marco Fork/Join** donde se añadirá la tarea para su ejecución.

13. Finalizamos con el **marco Fork/Join**.

14. Se esperará hasta que las tareas que se han lanzado finalicen su ejecución. Podremos comprobar que, aunque se haya producido la excepción, no se interrumpe aún la ejecución de la aplicación por la misma.

15. Comprobamos si alguna de las tareas ha provocado una **excepción no capturada** durante su ejecución mediante el método `isCompletedAbnormally()`. Si se ha producido la excepción se mostrará su resultado por la pantalla.

```java
...
// Check if the task has thrown an Exception. If it's the case, write it
// to the console
if (task.isCompletedAbnormally()) {
    System.out.printf("Main: An exception has ocurred\n");
    System.out.printf("Main: %s\n",task.getException());
}
...
```

17. Como paso final, se presentará el resultado de la ejecución de la tarea por la salida estándar.

## Ejercicios Propuestos

¿Se mostrará en la salida estándar el resultado final de la tarea? Razona la respuesta.

## Información Adicional
  
Se puede obtener el mismo resultado de que hemos visto en el ejemplo, si en vez de lanzar una excepción, utilizamos el método `completeExceptionally(.)` de la clase [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html). El código que debemos utilizar es el siguiente:

```java
...
Exception e = new Exception ("This task throws an Exception: "
                              + "Task from " + start + " to " + end);
completeExceptionally(e);
...
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbNDEwMzQ0MTg5XX0=
-->