# Reuniendo los resultados de las tareas

El **marco Fork/Join** proporciona la capacidad de las tareas que se ejecutan de devolver un resultado. Para ello tenemos la clase [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) para la implementación de las tareas. Esta clase hereda de la clase [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html) e implementa la interface [`Future`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Future.html) que proporciona el **marco Executor**.

Dentro de la tarea, utilizaremos la estructura recomendada por la documentación del *API de Java*:

```
...
If (problem size > default size) {
    task = divide(task);
    execute(task);
    groupResults();
    return result;
} else {
    resolve problem using another algorithm;
    return result;
}
...
```

Si la tarea tiene que resolver un problema cuyo tamaño es mayor al de referencia, dividiremos la tarea en diferentes subtareas y las ejecutaremos usando el **marco Fork/Join**. Cuando finalizan su ejecución, la tarea obtiene los resultados generados por todas las subtareas, agrupándolos, y devolviendo el resultado final. Por último, cuando la tarea inicial ejecutada en el marco finaliza su ejecución, se obtendrá su resultado que es efectivamente el resultado final del problema completo.

En este ejemplo, aprenderemos el uso de esta capacidad para resolver problemas mediante el **marco Fork/Join** desarrollando una aplicación que busca una palabra, concreta, en un documento. Para ello implementados dos tipos de tareas:

- Una *tarea de documento*, que va a buscar una palabra en un conjunto de líneas de un documento.

- Una *tarea de línea*, que va a buscar una palabra en una parte del documento.

Todas estas tareas van a devolver el número de ocurrencias de la palabra en la parte del documento o línea que procesen.

1. Para simular el documento definimos una clase llamada `DocumentMock`. En la clase tendremos un array de palabras para simular la generación del documento.

2. Implementamos el método `generateDocument(.)` que devolverá la estructura que simula el documento con una generación aleatoria de palabras. Además mostrará en la salida estándar el número de ocurrencias de la palabra que buscaremos para comprobar que la tarea funcionará correctamente. Revisar el código del ejemplo del guión de prácticas.

3. Ahora definimos la clase que representa la tarea de documento, llamada `DocumentTask` que hereda de la clase `RecursiveTask` *parametrizada* por el tipo `Integer` que será el tipo de valor devuelto por la tarea.

4. Necesitamos como variables de instancia:
	- La que permite almacenar el documento donde se buscará la palabra.
    - El número de línea de inicio y final que definirá el conjunto de líneas donde buscará la tarea.
    - La palabra a buscar.

5. El constructor de la clase asignará los valores apropiados a estas variables de instancia. Revisar el código del ejemplo del guión.

6. El método `compute()` debemos implementarlo para establecer la lógica de ejecución de la tarea. En nuestro caso, comprobamos si el conjunto de líneas en el que debemos buscar es menor de `10`. Si la respuesta es positiva se invocará el método `processLines(.)` que nos devolverá el número de ocurrencias de la palabra en ese conjunto de líneas.

```java
...
@Override
protected Integer compute() {
    Integer result=null;
    if (end-start<10){
        result=processLines(document, start,end,word);
...
```

7. En otro caso, dividimos el conjunto de líneas a tratar en dos y creamos dos nuevas tareas de documento para realizar el procesamiento. Y las ejecutamos mediante el método `invokeAll(.)`.

8. Cuando terminan las subtareas creadas anteriormente agrupamos los resultados mediante el método `groupResults(.)` y para finalizar devolvemos el resultado de la tarea.

```java
...
} else {
    int mid=(start+end)/2;
    DocumentTask task1=new DocumentTask(document,start,mid,word);
    DocumentTask task2=new DocumentTask(document,mid,end,word);
    invokeAll(task1,task2);
    try {
        result=groupResults(task1.get(),task2.get());
    } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
    }
}
return result;
...
```

9. La implementación del método `processLines(.)` recibe como parámetros el conjunto de líneas donde deberá buscar las ocurrencias de la palabra.

10. Para ello creamos un array de tareas de línea para cada una de las líneas del conjunto de búsqueda al que le pasaremos la palabra que deseamos buscar.

11. Añadiremos todas las tareas al **marco Fork/Join** mediante el método `invokeAll(.)`. Cuando todas las subtareas hayan finalizado se continuará con la ejecución del método. Una vez que han finalizado, se sumarán los resultados y se devolverá el valor.

```java
...
private Integer processLines(String[][] document, int start, int end,
                             String word) {
    List<LineTask> tasks=new ArrayList<LineTask>();
		
    for (int i=start; i<end; i++){
        LineTask task=new LineTask(document[i], 0, document[i].length, word);
        tasks.add(task);
    }
    invokeAll(tasks);
		
    int result=0;
    for (int i=0; i<tasks.size(); i++) {
        LineTask task=tasks.get(i);
        try {
            result=result+task.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    return new Integer(result);
}
...
```

12. Para procesar las líneas del documento definimos la tarea de línea, la clase se llama `LineTask` que hereda de la clase `RecursiveTask` que estará *parametrizada* por el tipo `Integer` que es el valor devuelto por la tarea.

13. Necesitaremos las siguientes variables de instancia:
	- La parte del documento que se procesará, que corresponderá a una inicialmente a una línea del documento.
	- El inicio y final de la parte de la línea que se deberá procesar.
	- La palabra que se buscará.
    
14. El constructor de la clase asignará los valores apropiados a las variables de instancia. Revisar el código de ejemplo del guión.

15. La lógica de la ejecución de la tarea estará en el método `compute()` que tenemos que implementar.

16. El tamaño de referencia para esta tarea será de `100` palabras. Si el conjunto es menor de `100` palabras se invoca el método `count(.)` que contará el número de ocurrencias de la palabra en ese espacio del documento.

17. En otro caso, se dividirá el conjunto entre dos y se creará una tarea para cada uno de ellos y se envía al **marco Fork/Join** mediante el método `invokeAll(.)`. Cuando finalizan las subtareas se agruparán los resultados de éstas y se devolverá el resultado.

```java
...
@Override
protected Integer compute() {
    Integer result=null;
    if (end-start<100) {
        result=count(line, start, end, word);
    } else {
        int mid=(start+end)/2;
        LineTask task1=new LineTask(line, start, mid, word);
        LineTask task2=new LineTask(line, mid, end, word);
        invokeAll(task1, task2);
        try {
            result=groupResults(task1.get(),task2.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    return result;
}
...
```

18. Para probar las tareas implementamos el método `main(.)` de la aplicación.

19. Simularemos el documento donde se buscará la palabra con `100` líneas y `1000` palabras por línea.

20. Creamos una tarea de documento a la que pasaremos el documento creado anteriormente. Luego se enviará al **marco Fork/Join** para obtener el número de ocurrencias de la palabra en el documento.

21. Mientras finaliza la ejecución de las tareas se mostrarán datos de la ejecución de las tareas en la salida estándar. Al finalizar la tarea principal se mostrará el resultado por la salida estándar. Revisar el código del guión de la práctica.

## Información Adicional

La clase [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.htm) proporciona otro método que a la finalización de una tarea devuelve el resultado, este es, el método `complete(.)`. Este método acepta un objeto del tipo utilizado en la *parametrización* de la clase `RecursiveTask` y devuelve este objeto como el resultado de la tarea cuando el método `join()` es invocado. Su uso es el recomendado para proporcionar los resultados de las tareas asíncronas.

Desde que la clase `RecursiveTask` implementa la interface `Future`, hay otra versión el método `get(.)`:

- `get(long timeout, TimeUnit unit)`: Esta versión del método `get(.)`, si el resultado de la tarea no se encuentra disponible, espera por el tiempo especificado en ella. Si el tiempo especificado finaliza y el resultado no está disponible aún, el método devolverá el valor `null` como resultado. La clase [`TimeUnit`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/TimeUnit.html) es una enumeración con las siguientes constantes: `DAYS`, `HOURS`, `MICROSECONDS`, `MILLISECONDS`, `MINUTES`, `NANOSECONDS` y `SECONDS`.


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc4MjE0MDczMV19
-->