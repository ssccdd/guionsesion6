# Ejecutando las tareas asíncronamente

Cuando se ejecuta una tarea [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html) en un marco [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html), se puede hacer de forma **síncrona** o **asíncrona**. Cuando utilizamos la forma **síncrona**, el método que envía las tareas al **marco Fork/Join** no retorna hasta que las tareas que se envió no termine su ejecución. Cuando utilizamos la forma **asíncrona**, el método que envía las tareas al **marco Fork/Join**  retorna inmediatamente, esto es, continúa con su ejecución.

Tenemos que tener presente esta gran diferencia entre los dos métodos. Cuando utilizamos el método sincronizado, la tarea que invoca uno de esos métodos (por ejemplo, el método `invokeAll(.)`) es suspendida hasta que las tareas enviadas al **marco Fork/Join** finalizan su ejecución. Esto permite a la clase `ForkJoinPool` usar el algoritmo *work-stealing* para asignar una nueva tarea al *hilo de trabajo* que ejecutaba la tarea suspendida. Al contrario, cuando utilizamos los métodos asíncronos (por ejemplo, el método `fork(.)`) la tarea continúa con su ejecución, esto es, la clase `ForkJoinPool` no puede utilizar el algoritmo *work-stealing* para incrementar el rendimiento de la aplicación. En este caso, sólo cuando se invoca el método `join(.)` o `get(.)` para esperar a la finalización de las tareas que previamente añadió al **marco Fork/Join**.

En este ejemplo, aprenderemos como utilizar los métodos **asíncronos** que tienen las clases `ForkJoinPool` y `ForkJoinTask` para la gestión de las tareas. Vamos a implementar un programa que buscará los ficheros que tengan una extensión concreta dentro de un directorio y todos sus subdirectorios. Diseñaremos una tarea que herede de `ForkJoinTask` para buscar en un directorio. Para cada subdirectorio, se enviará una nueva tarea al **marco Fork/Join** de forma **asíncrona**. Por cada fichero dentro del directorio, la tarea comprobará su extensión y lo añadirá a la lista de resultados si es procedente.

1. Definimos una clase llamada `FolderProcessor` que hereda de la clase [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) *parametrizada* por el tipo `List<String>` que será el resultado que devuelva de su ejecución.

2. Debemos establecer la `versión UID` de la clase. Esto es necesario porque la clase padre implementa la interface [`Serializable`](https://docs.oracle.com/javase/8/docs/api/java/io/Serializable.html).

3. Tenemos dos variables de instancia para la clase:
	- Una variable que almacena la trayectoria del directorio donde se iniciará la búsqueda.
	- Una variable que almacena la extensión que deben tener los ficheros que buscamos.
    
4. El constructor de la clase deberá inicializar estas variables de instancia. Revisa el código de ejemplo del guión.

5. Implementamos el método `compute()` para establecer la codificación de la tarea que se debe realizar.

6. Creamos una lista que nos permite almacenar los ficheros que cumplan el criterio de búsqueda de la tarea.

7. Creamos también una lista de tareas `FolderProcessor` para que podamos buscar en los subdirectorios, que contenga el directorio actual, por los ficheros que cumplan el criterio de búsqueda.

```java
...
@Override
protected List<String> compute() {
    List<String> list=new ArrayList<>();
    List<FolderProcessor> tasks=new ArrayList<>();
...
```

8. El siguiente paso es obtener la lista de elementos presentes en el directorio actual, tanto ficheros como subdirectorios. Para cada uno de los elementos presentes en la lista, si es un subdirectorio, creamos una nueva tarea `FolderProcessor` y los ejecutamos asíncronamente mediante el método `fork()`.

9. En otro caso, comparamos la extensión del fichero con la que estamos buscando mediante el método `checkFile(.)`, y si es igual, almacenamos la trayectoria completa en la lista de resultados creada anteriormente.

```java
...
    File file=new File(path);
    File content[] = file.listFiles();
    if (content != null) {
        for (int i = 0; i < content.length; i++) {
            if (content[i].isDirectory()) {
                // If is a directory, process it. Execute a new Task
                FolderProcessor task=new FolderProcessor(content[i].getAbsolutePath(), extension);
                task.fork();
                tasks.add(task);
            } else {
                // If is a file, process it. Compare the extension of the file and the extension
                // it's looking for
                if (checkFile(content[i].getName())){
                    list.add(content[i].getAbsolutePath());
                }
            }
        }
			
        // If the number of tasks thrown by this tasks is bigger than 50, we write a message
        if (tasks.size()>50) {
            System.out.printf("%s: %d tasks ran.\n",file.getAbsolutePath(),tasks.size());
        }
			
        // Include the results of the tasks
        addResultsFromTasks(list,tasks);
    }
return list;
...
```
 
 10. Si se crean más de `50` subtareas, presentamos un mensaje en la salida estándar para informar de esta circunstancia. En este caso, el rendimiento del programa puede verse afectado por el alto número de hilos presentes en el sistema.

11. Si una tarea ha añadido nuevas tareas al **marco Fork/Join** deberá recoger los resultados de éstas antes de que pueda devolver su propio resultado. Para ello utilizaremos el método `addReultsFromTaks(.)`. Como hemos utilizado el método asíncrono, debemos realizar este paso antes de poder terminar.

12. El método explorará cada una de las tareas que estén presentes en la lista de tareas que se han creado. Se esperará hasta la finalización de las tareas mediante el método `join()` y se añadirá el resultado devuelto a la lista de resultados.

```java
...
private void addResultsFromTasks(List<String> list,
    List<FolderProcessor> tasks) {
    for (FolderProcessor item: tasks) {
        list.addAll(item.join());
    }
}
...
```

13. Implementamos el método `main(.)` de la aplicación. En el método tenemos que crear el **marco Fork/Join**. También creamos tres tareas para buscar en tres directorios en concreto y los añadimos al marco.

```java
...
// Create the pool
ForkJoinPool pool=new ForkJoinPool();
		
// Create three FolderProcessor tasks for three diferent folders
FolderProcessor system=new FolderProcessor("C:\\Windows", "log");
FolderProcessor apps=new FolderProcessor("C:\\Program Files","log");
FolderProcessor documents=new FolderProcessor("C:\\Documents And Settings","log");
		
// Execute the three tasks in the pool
pool.execute(system);
pool.execute(apps);
pool.execute(documents);
...
```

14. Mientras las tareas no finalicen, mostramos una serie de datos relevantes por la salida estándar.

15. Finalizamos la ejecución del **marco Fork/Join**.

16. Recogemos los resultados de las tres tareas mediante el método `join()` y los mostramos por la salida estándar.

```java
...
// Write statistics of the pool until the three tasks end
do {
    System.out.printf("******************************************\n");
    System.out.printf("Main: Parallelism: %d\n",pool.getParallelism());
    System.out.printf("Main: Active Threads: %d\n",pool.getActiveThreadCount());
    System.out.printf("Main: Task Count: %d\n",pool.getQueuedTaskCount());
    System.out.printf("Main: Steal Count: %d\n",pool.getStealCount());
    System.out.printf("******************************************\n");
    try {
        TimeUnit.SECONDS.sleep(1);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
} while ((!system.isDone())||(!apps.isDone())||(!documents.isDone()));
		
// Shutdown the pool
pool.shutdown();
		
// Write the number of results calculate by each task
List<String> results;
		
results=system.join();
System.out.printf("System: %d files found.\n",results.size());
		
results=apps.join();
System.out.printf("Apps: %d files found.\n",results.size());
		
results=documents.join();
System.out.printf("Documents: %d files found.\n",results.size());
...
```

## Información Adicional

En este ejemplo, se ha utilizado el método `join()` para esperar a la finalización de las tareas y obtener sus resultados. También se podría haber utilizado una de las dos versiones del método `get()`:

 - `get()`: Esta versión del método devuelve el valor del método `compute()` si la tarea [`ForkFoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html) ha finalizado su ejecución, o espera hasta la finalización.

- `get(long timeout, TimeUnit unit)`: Esta versión del método, si el resultado de la tarea no está disponible, espera por el tiempo especificado. Si ese tiempo se cumple, y el resultado no está disponible, devuelve un valor `null`.

Hay dos diferencias principales entre los métodos `get()` y `join()`:

- El método [`join()`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html#join--) no puede ser interrumpido. Si se interrumpe el hilo que invoca el método `join()` se lanzará una excepción `InterruptedException`.

- Mientras que el método [`get()`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html#get--) devolverá una excepción `ExecutionException` si la tarea lanza una *excepción no capturada*, el método `join()` devolverá una excepción `RuntimeException`.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTY0MDU3ODczXX0=
-->