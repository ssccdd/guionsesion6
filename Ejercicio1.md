# Creando un conjunto Fork/Join

En este ejemplo, se aprenderá a utilizar de forma básica elementos del **marco Fork/Join**. Incluimos:

- La creación de un objeto [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html) para ejecutar las tareas.

- La creación de una subclase de [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html) para ejecutarse en el marco.

Las principales características del **marco Fork/Join** se utilizarán en este ejemplo y son las siguientes:

- Se crea un objeto `ForkJoinPool` mediante el constructor por defecto.

- Dentro de la tarea, se utilizará la estructura recomendada en la documentación del *API de Java*:

```
...
If (problem size > default size) {
    task = divide(task);
    execute(task);
} else {
    resolve problem using another algorithm;
}
...
```

- Se ejecutarán las tareas mediante un método sincronizado. Cuando una tarea ejecuta dos o más subtareas, espera hasta que finalicen. De esta forma, el hilo que está ejecutando esa tarea (*hilo de trabajo*) comprobará si otras tareas pueden ejecutarse, aprovechando completamente su tiempo de ejecución.

- Las tareas que se van a implementar no devuelven ningún resultado, por tanto la clase de partida será [`RecursiveAction`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveAction.html).

En este ejemplo, vamos a realizar una tarea que actualizará el precio de una lista de productos. La tarea inicial será la responsable de actualizar todos los elementos de la lista. Utilizaremos como tamaño de referencia `10` productos, si una tarea tiene que actualizar más de `10` productos, dividirá su lista asignada en dos partes y creará dos subtareas para actualizar los precios de sus respectivas partes.

1. Primero definimos una clase para representar los productos que tenemos. El nombre de la clase será `Product`.

2. Tendrá dos variables de instancia, una para representar el nombre del producto y otra para establecer el precio.

3. Necesitaremos los métodos de acceso para estas dos variables de instancia. El código necesario se puede revisar en el ejemplo del guión.

4. Para simular una lista de productos, creamos una clase que se llamará `ProductListGenerator`. La clase tendrá una lista de productos y su constructor creará una lista para un tamaño establecido. Revisar el código de ejemplo del guión.

5. Una vez que ya tenemos las dos clases necesarias para simular nuestra lista de productos creamos la clase que definirá nuestra tarea. Creamos una clase llamada `Task` que hereda de la clase `RecursiveAction`.

6. Debemos declarar la `versión UID` de la clase. Este elemento es necesario, porque la clase padre de `RecursiveAction`, la clase `ForkJoinTask`, implementa la interfaz [`Serializable`.](https://docs.oracle.com/javase/8/docs/api/java/io/Serializable.html) Revisar el código de ejemplo del guión.

7. Tendremos una variable de instancia que representará la lista de productos. También tendremos dos variables de instancia que representará el inicio y final de la lista de precios que deberá actualizar la tarea.

8. Por último, necesitaremos una variable de instancia para saber el valor con el que se incrementará el precio de cada elemento de la lista de productos. Todas estas variables de instancia deberán obtener su valor mediante el constructor de la clase. Revisar el código de ejemplo del guión.

9. Para establecer el código que debe realizar la tarea se tiene que implementar el método `compute()`. Siguiendo el esquema presentado al inicio de la documentación de este ejemplo, comprobamos si la lista está compuesta por menos de `10` productos. Si es así se invocará el método `updatePrieces()` que completará el trabajo de la tarea.

```java
...
/**
 * Method that updates the prices of the assigned products to the task
 */
private void updatePrices() {
    for (int i=first; i<last; i++){
        Product product=products.get(i);
        product.setPrice(product.getPrice()*(1+increment));
    }
}
...
```

10. En otro caso, deberemos dividir la lista entre dos y crear dos nuevas tareas para que se pueda actualizar la lista de precios de los productos. Y se enviará al **marco Fork/Join** mediante el método `invokeAll(.)`. De esta forma, la tarea esperará hasta la finalización de sus subtareas.

```java
...
/**
 * Method that implements the job of the task
 */
@Override
protected void compute() {
    if (last-first<10) {
        updatePrices();
    } else {
        int middle=(last+first)/2;
        System.out.printf("Task: Pending tasks: %s\n",getQueuedTaskCount());
        Task t1=new Task(products, first,middle+1, increment);
        Task t2=new Task(products, middle+1,last, increment);
        invokeAll(t1, t2);	
    }
}
...
```

11. Ahora implementamos el método `main(.)` de la aplicación para probar que la tarea actualiza correctamente la lista de precios.

12. Tenemos que tener un objeto que nos represente la lista de precios que la tarea deberá actualizar.

13. Creamos una tarea a la que pasaremos la lista de precios completa y el incremento que deberá aplicarse a la lista.

14. Creamos el objeto que representa al **marco Fork/Join** y enviamos la tarea.

```java
...
// Create a list of products
ProductListGenerator generator=new ProductListGenerator();
List<Product> products=generator.generate(10000);
		
// Craete a task
Task task=new Task(products, 0, products.size(), 0.20);
		
// Create a ForkJoinPool
ForkJoinPool pool=new ForkJoinPool();
		
// Execute the Task
pool.execute(task);
...
```

15. Presentamos por la salida estándar, cada `5` milisegundos, datos relativos al **marco Fork/Join** sobre la ejecución que se está realizando hasta que finalice.

16. Finalizamos el **marco Fork/Join** y comprobamos que la finalización de todas las tareas ha sido correcta.

17. Para finalizar comprobamos la lista por si alguno de los artículos no se ha actualizado su precio de la forma esperada. Todos los artículos deben tener el mismo precio y este es `10`. Revisar el código en el ejemplo del guión.

## Información Adicional

La clase [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html) proporciona otros métodos para ejecutar tareas en él. Los métodos son los siguientes:

- `execute (Runnable task)`: Esta es una nueva versión del método `execute(.)` presentado en el ejemplo. En este caso, se envía una objeto que implementa la interface `Runnable` a `ForkJoinPool`. Hay que tener presente que `ForkJoinPool` no utilizará el algoritmo *work-stealing* con el objeto `Runnable`. Sólo será utilizado con objetos `ForkJoinTask`.

- `invoke(ForkJoinTask<T> task)`: Mientras que el método `execute(.)` realiza una llamada asíncrona de la clase `ForkJoinPool`, como se ha presentado en el ejemplo, el método `invoke(.)` realiza una llamada síncrona a la clase `ForkJoinPool`. Esta llamada no retorna el control hasta que la tarea que se le ha pasado no finaliza su ejecución.

- También se puede utilizar los métodos `invokeAll(.)` e `invokeAny(.)` declarados en la interface `ExecutorService`. Estos métodos tienen como parámetro un objeto que implementa la interface `Callable`. La clase `ForkJoinPool` no utiliza el algoritmo *work-stealing* con estos objetos y por tanto es mejor para su ejecución utilizar un `Executor`.
    
La case `ForkJoinTask` también incluye otras versiones del método `invokeAll(.)` usado en el ejemplo. Estas versiones son las siguientes:

-   `invokeAll(ForkJoinTask<?>... tasks)`: Esta versión del método utiliza una lista variable de argumentos. Se pueden pasar como parámetros tantos objetos `ForkJoinTask` como se desee.
    
- `invokeAll(Collection<T> task)`: Esta versión del método acepta una colección (por ejemplo, un objeto `ArrayList`, un objeto `LinkedList`, o un objeto `TreeSet`) de objetos de un tipo genérico del tipo `T`. Este tipo genérico `T`  tiene que ser de la clase `ForkJoinTask` o una subclase.

Aunque la clase `ForkJoinPool` está diseñada para ejecutar objetos de la clase `ForkJoinTask`, se pueden ejecutar también objetos que implementan las interfaces `Runnable` y `Callable` directamente. También se puede utilizar el método `adpat(.)` de la clase `ForkJoinTask` que acepta objetos `Runnable` o `Callable` y devuelve un objeto `ForkJoinTask` de la ejecución de la tarea.


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTk5MzI1MjA3NF19
-->