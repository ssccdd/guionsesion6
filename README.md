[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS[^nota1]
## Sesión 6: Marco Fork/Join

Normalmente, cuando implementamos una pequeña aplicación concurrente en Java, definimos algunas clases que implementan la interface [`Runnable`](https://docs.oracle.com/javase/8/docs/api/java/lang/Runnable.html), y para que se ejecute, creamos un objeto de la clase [`Thread`](https://docs.oracle.com/javase/8/docs/api/java/lang/Thread.html) al que le asociaremos una instancia de esa primera clase. De esta forma controlamos la creación, ejecución y el estado de esos hilos en nuestra aplicación. En la versión de Java 5 se incluyó una mejora con las interfaces [`Executor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) además de clases que las implementan (por ejemplo, la clase [`ThreadPoolExecutor`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadPoolExecutor.html)).

El **marco Executor**  separa la creación de las tareas y su ejecución. Con ello, sólo tenemos que definir las clases que implementan la interface `Runnable` y las instancias las enviaremos a un objeto que implemente la interface `Executor`. De esta forma el objeto `Executor` se encargará de la gestión de los hilos de forma interna y a parte del programador. 

Con la actualización de Java 7 se incluye una nueva mejora a la interface [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) orientada a un tipo específico de problemas. Esto es el **marco Fork/Join**. Este marco está diseñado para resolver problemas que pueden dividirse en tareas más pequeñas usando la estrategia *divide y vencerás*. Dentro de la tarea, se comprueba el tamaño del problema que se quiere resolver, si es demasiado grande a un tamaño establecido, se dividirá en tareas menores que son ejecutadas utilizando el marco. Si la tarea ya ha alcanzado el tamaño establecido, o menor, la tarea resuelve el problema directamente en la tarea y entonces, opcionalmente, devuelve el resultado. Se puede ver gráficamente en la figura:

  
![](https://lh3.googleusercontent.com/F8qEP-YhoSF9CCPhjB3bYnwUscE9Tr_bd5IZCXyGdjIaUYW8RbBf9VOz3MSndFxhWFGdtxUpgAtXJWqcV3c1Zqwyza93ihtcBVkWOqGZjCdNGDuiIuJ23pG3FzEC9fBY42OY5vvb)

No hay una forma genérica para determinar el tamaño que debe tener un problema para poder dividirse en tareas más pequeñas, eso dependerá del problema en concreto que se quiera resolver. Podemos utilizar el número de elementos para procesar las tareas y estimar el tiempo que nos llevará ejecutarlas para determinar el tamaño necesario en el que decidiremos si dividir la tarea o no. Se pueden probar diferentes tamaños para elegir el que mejor se ajusta al problema. Podemos utilizar el **marco Fork/Join** como un caso especial del **marco Executor**.

El marco se basa en las siguientes dos operaciones:

- La operación `fork`: Cuando dividimos la tarea en tareas más pequeñas y las ejecutamos utilizando el marco.

- La operación `join`: Cuando la tarea espera a la finalización de las tareas que ha creado.
    
La mayor diferencia entre el **marco Fork/Join** y el **marco Executor** es el algoritmo *work-stealing*. A diferencia del **marco Executor**, cuando una tarea está esperando a la finalización de las subtareas que ha creado usando la operación `join`, el hilo que está ejecutando esa tarea (*hilo de trabajo*) comprueba para otras tareas que aún no están ejecutándose y comienza su ejecución. De esta forma, el hilo consigue sacar el máximo provecho de su tiempo de ejecución, de esta forma se mejora el rendimiento de la aplicación.

Para alcanzar este objetivo, las tareas ejecutadas en el marco **Fork/Join** tiene las siguientes limitaciones:

- Las tareas sólo pueden utilizar las operaciones `fork` y `join` como mecanismo de sincronización. Si se utilizan otros mecanismos de sincronización, los *hilos de trabajo* no pueden ejecutar otras tareas cuando esté en una operación de sincronización. Por ejemplo, si un hilo se suspende en un **marco Fork/Join**, el hilo de trabajo que está ejecutando esta tarea no puede ejecutar otra mientras dure el tiempo de suspensión.

- Las tareas no deben realizar operaciones de **Entrada/Salida**, como la escritura o lectura de datos de un fichero.

- Las tareas no pueden lanzar *excepciones capturadas*. No puede incluir el código necesario para su procesamiento.

El **marco ForkJoin** está formado por las siguientes dos clases:

- [`ForkJoinPool`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html): Implementa la interface `ExecutorService` y el algoritmo *work-stealing*. Gestiona los hilos de trabajo  y proporciona información sobre el estado de las tareas y su ejecución.

- [`ForkJoinTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinTask.html): Es la clase de partida que se ejecutará en `ForkJoinPool`. Proporciona los mecanismos para ejecutar las operaciones `fork` y `join` dentro de las tareas y los métodos para controlar el estado de las tareas. En general, para implementar las tareas **Fork/Join**, implementaremos dos subclases de otras dos subclases de esta clase: [`RecursiveAction`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveAction.html) para tareas que no tienen que devolver un resultado y [`RecursiveTask`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/RecursiveTask.html) para las que tienen que devolver un resultado.

Los siguientes ejemplos muestran algunas de las capacidades del **marco Fork/Join**:

1. [Creando un conjunto **Fork/Join**.](https://gitlab.com/ssccdd/guionsesion6/-/blob/master/Ejercicio1.md)
2. [Reuniendo los resultados de las tareas.](https://gitlab.com/ssccdd/guionsesion6/-/blob/master/Ejercicio2.md)
3. [Ejecutando las tareas asíncronamente.](https://gitlab.com/ssccdd/guionsesion6/-/blob/master/Ejercicio3.md)
4. [Lanzando excepción en las tareas.](https://gitlab.com/ssccdd/guionsesion6/-/blob/master/Ejercicio4.md)
5. [Cancelando una tarea.](https://gitlab.com/ssccdd/guionsesion6/-/blob/master/Ejercicio5.md)

---
[^nota1]: El guión se extrae del libro *Java 7 Concurrency Cookbook* que se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU3Njc2MDkyMF19
-->